<?php
require "vendor/autoload.php";
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

// get posted data into local variables
$EmailT = "dispatch@gomygreen.com";
$Subject = "NOT PAID: myGREEN Booking";
$FName = Trim(stripslashes($_POST['tfname']));
$LName = Trim(stripslashes($_POST['tlname']));
$Phone = Trim(stripslashes($_POST['tphone']));
$From= Trim(stripslashes($_POST['tfrom'])); 
$To= Trim(stripslashes($_POST['tto']));  
$Email = Trim(stripslashes($_POST['temail'])); 
$Type = Trim(stripslashes($_POST['ttype']));
$Passenger = Trim(stripslashes($_POST['tpassenger']));
$Date = Trim(stripslashes($_POST['tdate']));
$Time = Trim(stripslashes($_POST['hours'] . ' ' . $_POST['minutes'] . ' ' . $_POST['ampm'])); 
 

// validation
$validationOK=true;
if (Trim($Email)=="") $validationOK=false;

if (!$validationOK) { 
echo "Thank You For Contacting Us Our Representative Will Contact You As Soon As Possible.";
 
  exit;
}

// prepare email body text
$Body = "";
$Body .= "Customer First Name:                       ";
$Body .= $FName;
$Body .= "\n";
$Body .= "Customer Last Name:                        ";
$Body .= $LName;
$Body .= "\n";
$Body .= "Number Of Passengers:                      ";
$Body .= $Passenger;
$Body .= "\n";
$Body .= "Customers Email Address:                   ";
$Body .= $Email;
$Body .= "\n";
$Body .= "Customer Phone Number:                     ";
$Body .= $Phone;
$Body .= "\n";
$Body .= "Pickup Address:                            ";
$Body .= $From;
$Body .= "\n";
$Body .= "Drop-off Address:                          ";
$Body .= $To;
$Body .= "\n";
$Body .= "Vehicle Preference:                        ";
$Body .= $Type;
$Body .= "\n";
$Body .= "Pick Up Date: [MM / DD / YYYY]             ";
$Body .= $Date;
$Body .= "\n";
$Body .= "Pick Up Time: [HR MIN 0=AM 1=PM]           ";
$Body .= $Time;
$Body .= "\n";

 
$client = new Client();

$body =['{
  "psgInfo":{
     "phone":"+84905997022",
     "firstName":"Vinh",
     "lastName":"Tran",
     "email":"vinhjs@gmail.com",
     "creditInfo":{
        "cardNumber":"4111111111111111",
        "cardHolder":"vinh",
        "expiredDate":"12/2018",
        "cvv":"123",
        "storeCard":true,
        "postalCode":"12345",
        "street":"",
        "city":"",
        "state":"",
        "country":""
     }
  },
  "request":{
     "pickup":{
        "address":"2 Tran Phu, Da Nang",
        "geo":[
           108.222404,
           16.082397
        ],
        "timezone":"Asia/Saigon"
     },
     "destination":{
        "address":"2 Quang Trung, Da Nang",
        "geo":[
           108.222365,
           16.075347
        ],
        "timezone":"Asia/SaiGon"
     },
     "pickUpTime":"Now",
     "vehicleTypeRequest":"BlackCar",
     "type":0,
     "paymentType":2,
     "note":"hoang test api",
     "promo":"1111",
     "rideSharing":false,
     "tip":10,
     "packageRateId":"",
     "moreInfo":{
        "flightInfo":{
           "airline":"HVN",
           "flightNumber":"HVN259",
           "type":-1
        }
     }
  },
  "corporateInfo":{
     "division":"test",
     "managerEmail":"test@gmail.com",
     "corpId":"123",
     "managerName":"test corp",
     "costCentre":"abc123",
     "department":"aaa",
     "corporateId":"58edf498e4b0d2900ecf5391",
     "clientCaseMatter":"abc",
     "chargeCode":"123"
  },
  "dispatch3rd":false
}'];
$response = $client->post( "https://dispatch.qupworld.com/api/v2/agent/booking/create" , [
  
   'headers' =>
      [
         'Authorization' => 'Bearer 4fd27ad98f15eb2c63e49b6b8b6b2709f6a4ee23',
         'Content-Type'=>'application/json'
      ],$body
] );





// send email 
// $success = mail($EmailT, $Subject, $Body, "From: <$Email>");
		
// // redirect to success page 
// if ($success){
//   print "<meta http-equiv=\"refresh\" content=\"0;URL=confirm.html\">";
// }
// else{
//   print "<meta http-equiv=\"refresh\" content=\"0;URL=index.html\">";
// }
?>